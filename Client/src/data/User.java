package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class User implements Serializable{
    private String nick;
    private String password;
    private List<User> friends;
    private List<User> enemies;

    public User(){
        this.friends = new ArrayList<>();
        this.enemies = new ArrayList<>();
    }

    public User(String name, String password, List<User> friends, List<User> enemies){
        this.nick = name;
        this.password = password;
        this.friends = friends;
        this.enemies = enemies;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<User> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<User> enemies) {
        this.enemies = enemies;
    }

    public void addFriend(User user){
        this.friends.add(user);
    }
}
