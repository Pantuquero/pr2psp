package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChatRoom extends JDialog {
    private JPanel contentPane;
    private JButton btSend;
    private JButton btLeave;
    public JTextArea taChat;
    private JTextField tfMessage;
    private JLabel stateBar;

    private MainWindow mainWindow;

    public ChatRoom() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btSend);

        stateBar.setText("Your friend is here! speak with him!");

        btSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
    }
//TODO EXIT
    private void sendMessage(){
        if(!tfMessage.getText().equals("")){
            mainWindow.output.println("newmessage:" + mainWindow.lbUser.getText()  + ":" + tfMessage.getText());
        }else{
            stateBar.setText("Write something to say to your friend!");
        }
    }

    public static void main(String[] args) {
        ChatRoom dialog = new ChatRoom();
        dialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }
}
