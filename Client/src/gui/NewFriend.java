package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class NewFriend extends JDialog {
    private JPanel contentPane;
    private JButton btNew;
    private JButton btCancel;
    private JTextField tfSearch;
    private JButton btSearch;
    private JTextPane tpMessage;

    private MainWindow mainwindow;

    public NewFriend() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btSearch);
        tpMessage.setEditable(false);

        btSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    searchUser();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    newFriend();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void searchUser() throws IOException, ClassNotFoundException {
        if(!tfSearch.getText().equals("")){
            MainWindow.output.println("searchfriend:" + tfSearch.getText());
            if(MainWindow.input.readLine().contains("true")){
                this.tpMessage.setText("User found! click on \"New\" to add him to your contact list");
            }else{
                this.tpMessage.setText("User not found");
            }
        }else{
            tpMessage.setText("You must fill in the user's nick first!");
        }
    }

    private void newFriend() throws IOException, ClassNotFoundException {
        if(!tfSearch.getText().equals("")){
            MainWindow.output.println("searchfriend:" + tfSearch.getText());
            String answer = MainWindow.input.readLine();
            if(answer.contains("true")){
                MainWindow.output.println("addfriend:" + tfSearch.getText());
                setVisible(false);
            }else{
                this.tpMessage.setText("User not found");
            }
        }else{
            tpMessage.setText("You must fill in the user's nick first!");
        }
    }

    public static void main(String[] args) {
        NewFriend dialog = new NewFriend();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public void setMainwindow(MainWindow mainwindow) {
        this.mainwindow = mainwindow;
    }
}
