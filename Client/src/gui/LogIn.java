package gui;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

public class LogIn extends JDialog {
    private JPanel contentPane;
    private JButton btLogIn;
    private JButton btRegister;
    private JTextField tfNick;
    private JPasswordField psPassword;
    private JTextPane tpMessage;
    private JButton btCancel;

    private String localUser;
    private MainWindow mainWindow;

    public LogIn() throws IOException {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btLogIn);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        testConnection();

        btLogIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    getIn();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    register();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private void getIn() throws IOException, ClassNotFoundException {
        if(!tfNick.getText().equals("") && !psPassword.getText().equals("")){
            String order = "login:" + tfNick.getText() + ":" + psPassword.getText();
            MainWindow.output.println(order);

            String go = MainWindow.input.readLine();
            if(go.equals("go")){
                go = MainWindow.input.readLine();
            }
            if(go.equals("true")){
                this.localUser = mainWindow.input.readLine();
                setVisible(false);
            }else{
                this.tpMessage.setText("Incorrect username/password");
            }
        }else{
            tpMessage.setText("Please fill in the two fields");
        }
    }

    private void register() throws IOException, ClassNotFoundException {
        if(!tfNick.getText().equals("") && !psPassword.getText().equals("")){
            String order = "register:" + tfNick.getText() + ":" + psPassword.getText();
            MainWindow.output.println(order);
            if(MainWindow.input.readLine().equals("true")){
                this.localUser = mainWindow.input.readLine();
                setVisible(false);
            }else{
                this.tpMessage.setText("User already registered");
            }
        }else{
            tpMessage.setText("Please fill in the two fields");
        }
    }

    private void testConnection() throws IOException {
        if(MainWindow.socket!=null && MainWindow.input.readLine().equals("go")){
            this.tpMessage.setText("Please introduce your username and password or register to continue.");
        }else{
            this.tpMessage.setText("Connection refused, please restart the client.");
            this.btCancel.setText("Exit");
            this.btLogIn.setVisible(false);
            this.btRegister.setVisible(false);
            this.tfNick.setEnabled(false);
            this.psPassword.setEnabled(false);
        }
    }

    public static void main(String[] args) throws IOException {
        LogIn dialog = new LogIn();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public String getLocalUser() {
        return localUser;
    }

    public void setLocalUser(String localUser) {
        this.localUser = localUser;
    }

    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }
}
