package gui;

import threads.ChatListener;
import threads.FriendInfoUpdater;
import threads.ServerOnlineChecker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class MainWindow {
    private JPanel panel1;
    public JLabel stateBar;
    public JLabel lbUser;
    private JTable tbFriends;

    private int port;
    public static Socket socket;
    public static PrintWriter output;
    public static BufferedReader input;

    private JMenuItem exitItem;
    private JMenuItem newContactItem;
    private JMenuItem chatWithFriendItem;

    public DefaultTableModel friendsTableModel;

    public MainWindow(){
        this.port = 1992;
        this.exitItem = new JMenuItem("Exit");
        this.newContactItem = new JMenuItem("New");
        this.chatWithFriendItem = new JMenuItem("Chat");

        connect();
        logIn();

        try {
            this.lbUser.setText(getLocalUserName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        setFriendsTableModel();
        loadFriendsTable();

        new ServerOnlineChecker(this).start();
        new FriendInfoUpdater(this).start();
        new ChatListener(this).start();

        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(-1);
            }
        });
        newContactItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newContact();
            }
        });
        chatWithFriendItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendChatRequest();
            }
        });
    }

    private String getLocalUserName() throws IOException {
        output.println("getlocalusername");
        return  input.readLine();
    }

    private void sendChatRequest(){
        if(tbFriends.getSelectedRow()!=-1){
            if(friendsTableModel.getValueAt(tbFriends.getSelectedRow(), 1).toString().equals("Connected")){
                output.println("chatrequest:" + String.valueOf(tbFriends.getValueAt(tbFriends.getSelectedRow(), 0)));
            }else{
                stateBar.setText("Your friend is disconnected");
            }
        }else{
            stateBar.setText("Select one friend first");
        }
    }

    private void newContact(){
        NewFriend newFriend = new NewFriend();
        newFriend.setMainwindow(this);
        newFriend.setSize(300, 200);
        newFriend.setVisible(true);
        loadFriendsTable();
    }

    private ArrayList<String> getFriends() throws IOException {
        output.println("getlocaluserfriends");
        String[] answer = input.readLine().split(":");
        ArrayList<String> lista = new ArrayList<>();
        for(int i=0; i<answer.length;i++){
            lista.add(answer[i]);
        }
        return lista;
    }

    public synchronized void loadFriendsTable(){
        ArrayList<String> friends = new ArrayList<>();
        try {
            friends = getFriends();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(friends.size()==0)
            return;

        friendsTableModel.setNumRows(0);
        for(String friend : friends){
            Object[] row = new Object[]{
                friend,
                "Disconnected"
            };
            friendsTableModel.addRow(row);
        }
    }

    private void setFriendsTableModel(){
        this.friendsTableModel = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        friendsTableModel.addColumn("Nick");
        friendsTableModel.addColumn("Connected");
        this.tbFriends.setModel(friendsTableModel);
    }

    private void logIn(){
        LogIn logIn = null;
        try {
            logIn = new LogIn();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logIn.setSize(300,200);
        logIn.setMainWindow(this);
        logIn.setVisible(true);
    }

    public JMenuBar getMenuBar(){
        JMenuBar menuBar = new JMenuBar();

        JMenu optionsMenu = new JMenu("Options");
        JMenu contactsMenu = new JMenu("Contacts");

        contactsMenu.add(this.newContactItem);
        contactsMenu.add(this.chatWithFriendItem);
        menuBar.add(contactsMenu);

        optionsMenu.add(exitItem);
        menuBar.add(optionsMenu);

        return menuBar;
    }

    public void connect(){
        try {
            this.socket = new Socket("localhost" ,port);
            this.output = new PrintWriter(socket.getOutputStream(), true);
            this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println("Connection refused");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Messenger");
        MainWindow mainWindow = new MainWindow();
        frame.setContentPane(mainWindow.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(mainWindow.getMenuBar());
        frame.pack();
        frame.setVisible(true);
    }
}
