package threads;

import gui.MainWindow;

import java.io.IOException;
import java.net.SocketException;

public class ServerOnlineChecker extends Thread {
    MainWindow mainWindow;

    public ServerOnlineChecker(MainWindow mainWindow){
        this.mainWindow = mainWindow;
    }

    @Override
    public void run(){
        while(true){
            try {
                Thread.sleep(4000);
                mainWindow.output.println("checkconnection");
                if(mainWindow.input.readLine().equals("true")){
                    mainWindow.stateBar.setText("Server online");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (SocketException e){
                mainWindow.stateBar.setText("Server offline");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
