package threads;

import gui.MainWindow;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

public class FriendInfoUpdater extends Thread {
    private MainWindow mainWindow;
    private ArrayList<String> connectedUsers = new ArrayList<>();

    public FriendInfoUpdater(MainWindow mainWindow){
        this.mainWindow = mainWindow;
    }

    @Override
    public void run(){
        while(true){
            try {
                Thread.sleep(1000);

                mainWindow.output.println("checkfriends");
                String connectedUsersString = mainWindow.input.readLine();
                String[] connected = connectedUsersString.split(":");

                connectedUsers = new ArrayList<>();
                for(int i=0;i<connected.length;i++){
                    connectedUsers.add(connected[i]);
                }

                setConnectedFriends();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (StreamCorruptedException e){

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void setConnectedFriends(){
        for(int i=0; i<mainWindow.friendsTableModel.getRowCount(); i++){
            for(String user : connectedUsers){
                if(mainWindow.friendsTableModel.getValueAt(i,0).toString().equals(user)){
                    mainWindow.friendsTableModel.setValueAt("Connected", i, 1);
                    break;
                }else{
                    mainWindow.friendsTableModel.setValueAt("Disconnected", i, 1);
                }
            }
        }
    }
}
