package threads;

import gui.ChatRoom;
import gui.MainWindow;

import java.io.IOException;

public class ChatMessageReciever extends Thread {
    private MainWindow mainWindow;
    private ChatRoom chatRoom;
    private boolean exit = false;

    public ChatMessageReciever(MainWindow mainWindow, ChatRoom chatRoom){
        this.mainWindow = mainWindow;
        this.chatRoom = chatRoom;
    }

    @Override
    public void run(){
        while(!exit){
            try {
                Thread.sleep(1000);

                String[] message = mainWindow.input.readLine().split(":");
                switch (message[0]){
                    case "newmessage":
                        chatRoom.taChat.append(message[1] + ": " + message[2] + "\n");
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setExit(boolean exit) {
        this.exit = exit;
    }
}
