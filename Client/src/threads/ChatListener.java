package threads;

import gui.ChatRoom;
import gui.MainWindow;

import java.io.IOException;

public class ChatListener extends Thread {
    MainWindow mainWindow;

    public ChatListener(MainWindow mainWindow){
        this.mainWindow = mainWindow;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(1000);
                MainWindow.output.println("existschatrequest");
                String answer = MainWindow.input.readLine();
                if(answer.equals("true")){
                    ChatRoom chatRoom = new ChatRoom();
                    chatRoom.setSize(800,600);
                    chatRoom.setMainWindow(mainWindow);
                    chatRoom.setVisible(true);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
