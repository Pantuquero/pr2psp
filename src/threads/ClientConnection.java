package threads;

import data.User;
import main.Messenger_server;
import main.Server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientConnection extends Thread {
    public Socket socket;
    private String IP;
    public PrintWriter output;
    private BufferedReader input;

    private Server server;
    private User localUser;
    private boolean chatRequested = false;
    private ClientConnection userWhoWantsToChat;

    public ClientConnection(Socket socket, Server server) throws IOException {
        this.socket = socket;
        this.server = server;

        this.IP = String.valueOf(this.socket.getInetAddress().getHostAddress());

        output = new PrintWriter(this.socket.getOutputStream(), true);
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        try {
            authentification();
            mainProcess();
        } catch (IOException e) {
            System.out.println("ClientConnection: " + this.getIP() + " had an unknown error");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private synchronized void mainProcess() throws IOException, ClassNotFoundException {
        while (true){
            String inputString = input.readLine();
            String[] order = inputString.split(":");
            switch (order[0]){
                case "searchfriend":
                    searchUser(order);
                    break;
                case "addfriend":
                    addFriend(order);
                    break;
                case "checkconnection":
                    answerToConnectionChecker();
                    break;
                case "checkfriends":
                    connectedUsers();
                    break;
                case "chatrequest":
                    chatRequest(order);
                    break;
                case "existschatrequest":
                    existsChatRequest();
                    break;
                case "getlocalusername":
                    getUserName();
                    break;
                case "getlocaluserfriends":
                    getUserFriends();
                    break;
            }
        }
    }

    private void getUserFriends(){
        String answer = new String();
        for(User user : localUser.getFriends()){
            answer += user.getNick() + ":";
        }
        output.println(answer);
    }

    private void getUserName(){
        output.println(localUser.getNick());
    }

    private void existsChatRequest(){
        if(chatRequested){
            output.println("true");
            this.chatRequested = false;
            try {
                chat();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            output.println("false");
        }
    }

    private void chat() throws IOException {
        String[] message = input.readLine().split(":");
        //TODO
    }

    private void chatRequest(String[] order){
        for(ClientConnection clientConnection : Messenger_server.connectionList){
            if(clientConnection.getLocalUser().getNick().equals(order[1])){
                clientConnection.setChatRequested(true);
                clientConnection.setUserWhoWantsToChat(this);

                setChatRequested(true);
                userWhoWantsToChat = clientConnection;
                return;
            }
        }
    }

    private void connectedUsers() throws IOException {
        String connectedUsers = null;
        for(ClientConnection clientConnection : Messenger_server.connectionList){
            connectedUsers += clientConnection.getLocalUser().getNick() + ":";
        }
        output.println(connectedUsers);
    }

    private void answerToConnectionChecker(){
        output.println("true");
    }

    private synchronized void searchUser(String[] order){
        output.println(server.comprobateUser(order[1], null));
    }

    private void addFriend(String[] order) throws IOException {
        if(server.comprobateUser(order[1], null)){
            this.localUser.addFriend(server.getUser(order[1]));
            server.addUserFriends(this.localUser);
        }
    }

    private void authentification() throws IOException {
        Boolean ok = false;
        String[] orderSplit;
        this.output.println("go");

        while(!ok){
            String order = input.readLine();
            orderSplit = order.split(":");
            User user;
            switch (orderSplit[0]){
                case "login":
                    ok = this.server.comprobateUser(orderSplit[1], orderSplit[2]);

                    if(ok){
                        user = this.server.getUser(orderSplit[1]);
                        for(ClientConnection connection : Messenger_server.connectionList){
                            if(connection.getLocalUser()!=null){
                                if(connection.getLocalUser().getNick().equals(user.getNick())){
                                    output.println(false);
                                    return;
                                }
                            }
                        }
                        output.println("true");
                        this.localUser = user;
                        output.println(user.getNick());
                    }else
                        output.println("false");
                    break;
                case "register":
                    ok = this.server.comprobateUser(orderSplit[1], null);

                    if(!ok){
                        ok = true;
                        user = new User(orderSplit[1], orderSplit[2], new ArrayList<User>(), new ArrayList<User>());
                        this.localUser = user;
                        server.newModifyUser(user);
                        output.println("true");
                        output.println(this.server.getUser(orderSplit[1]).getNick());
                    }else{
                        ok = false;
                        output.println("false");
                    }
                    break;
            }
        }
    }

    public String getIP() {
        return IP;
    }

    public User getLocalUser() {
        return localUser;
    }

    public void setLocalUser(User localUser) {
        this.localUser = localUser;
    }

    public boolean isChatRequested() {
        return chatRequested;
    }

    public void setChatRequested(boolean chatRequested) {
        this.chatRequested = chatRequested;
    }

    public ClientConnection getUserWhoWantsToChat() {
        return userWhoWantsToChat;
    }

    public void setUserWhoWantsToChat(ClientConnection userWhoWantsToChat) {
        this.userWhoWantsToChat = userWhoWantsToChat;
    }
}
