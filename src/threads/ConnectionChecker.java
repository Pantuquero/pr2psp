package threads;

import main.Messenger_server;

import java.util.Iterator;
import java.util.List;

public class ConnectionChecker extends Thread {

    @Override
    public void run(){
        try {
            while (true){
                Thread.sleep(5000);

                ClientConnection clientConnection;
                Iterator<ClientConnection> iterator = Messenger_server.connectionList.listIterator();
                while (iterator.hasNext()){
                    clientConnection = iterator.next();
                    if(!clientConnection.isAlive()){
                        iterator.remove();
                        System.out.println("ConnectionChecker: " + clientConnection.getIP() + " disconnected");
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
