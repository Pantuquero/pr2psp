package main;

import threads.ClientConnection;
import threads.ConnectionChecker;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Messenger_server {
    private Server server;
    private ClientConnection clientConnection;
    public static List<ClientConnection> connectionList;
    private ConnectionChecker connectionChecker;

    public Messenger_server() throws IOException {
        this.server = new Server(1992);
        this.clientConnection = null;
        this.connectionList = new ArrayList<>();

        this.server.connect();

        this.connectionChecker = new ConnectionChecker();
        connectionChecker.start();

        System.out.println("Messenger_server: Server initialized");
        listen();
    }

    private void listen(){
        try {
            while(server.isConnected()){
                this.clientConnection = new ClientConnection(server.listen(), server);
                if(connectionList.size()!=0){
                    for(ClientConnection con : this.connectionList){
                        if(!con.getIP().equals(clientConnection.getIP())){
                            clientConnection.output.println("go");
                            clientConnection.start();
                            connectionList.add(this.clientConnection);
                            System.out.println("Messenger_server: Connection stablished with " + clientConnection.getIP());
                            break;
                        }else{
                            clientConnection.output.println("denied");
                            System.out.println("Messenger_server: Connection " + clientConnection.getIP() + " refused, line is busy.");
                        }
                    }
                }else{
                    clientConnection.start();
                    connectionList.add(this.clientConnection);
                    System.out.println("Messenger_server: Connection stablished with " + clientConnection.getIP());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]){
        try {
            new Messenger_server();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
