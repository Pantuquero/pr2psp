package main;

import com.db4o.Db4o;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectSet;
import data.User;
import sun.security.util.Password;
import util.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private int port;
    private ServerSocket serverSocket;

    public Server(int port){
        this.port = port;
    }

    public synchronized void newModifyUser(User user){
        Db.db.store(user);
        Db.db.commit();
    }

    public synchronized void addUserFriends(User user){
        Db.db.store(user.getFriends());
        Db.db.commit();
    }

    public synchronized void deleteUser(User user){
        Db.db.delete(user);
        Db.db.commit();
    }

    public synchronized User getUser(String nick){
        User user = new User();
        user.setNick(nick);
        List<User> userList = Db.db.queryByExample(user);
        user = userList.get(0);

        return user;
    }

    public synchronized boolean comprobateUser(String nick, String password){
        User user = new User();
        user.setNick(nick);

        if(password!=null)
            user.setPassword(password);

        List<User> userList = Db.db.queryByExample(user);
        if(userList.size()==1){
            return true;
        }

        return false;
    }

    private void connectWithDB(){
        Db.db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), Constants.DB_NAME);
    }

    public void connect() throws IOException {
        connectWithDB();
        this.serverSocket = new ServerSocket(this.port);
    }

    public void disconnect() throws IOException {
        this.serverSocket.close();
    }

    public boolean isConnected(){
        return !this.serverSocket.isClosed();
    }

    public Socket listen() throws IOException {
        return this.serverSocket.accept();
    }
}
